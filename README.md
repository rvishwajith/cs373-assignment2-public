CS N373 Assignment 2

## Authors
Rohith Vishwajith
Hai Hoang

## Steps
In this assignment you must work with a partner to perform merge request.

_Canvas Prerequisites:_
- Click on People in the sidebar.
- Click on the tab MergeRequest.
- Both of you must join the same group.
Then, only one of you will submit the assignment.

_Assignment Instructions:_
(1) Rohith: Login to your GitLab account and create a public repo (with a distinguished name
so that your partner can easily find it).

(2) Rohith: On your local machine (i.e., your laptop)
(2.1) Clone the repo to your local machine using the command "git clone url_of_public_repo"
(2.2) Move into the cloned directory
(2.3) Within the clonned directory, add a new file, myfile.txt with some text of your choice.
(2.4) Run the command "git status". What do you see?
(2.5) Run the commands "git add myfile.txt"
(2.6) Run the command "git commit -m "first commit"
(2.7) Run the command "git status" again.
What do you see?
(2.8) Push your changes to remote repo using the command "git push origin main"
What is "origin"? what is "main"?
Working area -> Staging area -> local repo
(3) Rohith: On your GitLab account, add Hai as a collaborator to your GitLab repo (give them "Guest" access level).
GitLab > "Settings" > "Members".

(4) Hai: On your GitLab account
(4.1) Fork Rohith's repo into your namespace
Locate Rohith's public repo
- Login to your GitLab account
- Click on "Explore projects", then click on "All"
- Search for Rohith's public repo
- Click on Fork: You’ll be asked to Select a namespace to fork the project, click on your namespace.

(5) Hai: On your local machine
(5.1) *Clone* your partner's (forked) repo to your local machine
(5.2) Create a dev branch on your local machine using the command: "git checkout -b dev"
(5.3) Create a new file, myfile1.txt, inside your partner's cloned repo
(5.4) Add, commit and push (similar to what we did in steps (2.5), (2.6) and (2.8))
(5.5) Push to "dev" branch on remote "git push origin dev"
(5.6) Merge dev into main
- Switch to main: "git checkout main"
- Merge dev: "git merge dev"

(5.7) Push to "main" branch on remote
"git push origin main"

(6) Hai: do a "merge request" to merge your changes to the main branch (i.e., Rohith's repo)
(6.1) On your GitLab account, select the forked repo.
(6.2) select "Merge requests" item from the menu to the left.
(6.3) Select "New merge request".
(6.4) Select the "source branch" to be "main".
(6.5) Hit "compare branches and continue".
(6.6) Hit "Submit merge request" ("Approve" is optional).
(6.7) Ask Rohith to check their account and accept the merge request.

Submission: Upload (preferably one pdf file that has all of the following) (1) a screenshot of the GitLab screen right after you accept the merge request and upload it to this assignment. The screenshot must be taken after Rohith accepts the merge request and must show the successful acceptance of the request and (2) a git log for each branch.

Optional exercise:

(7) Now, switch the roles (Hai becomes Rohith) and do the assignment again.